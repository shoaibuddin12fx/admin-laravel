<?php

// Code within app\Helpers\Helper.php

namespace App\Helpers;

class Helper
{
    public static function clean($string) {
        return preg_replace('/[^A-Za-z0-9 ]/', '', $string); // Removes special chars.
    }

    public static function split_name($name) {

        $name = clean($name);
        $parts = array();

        while ( strlen( trim($name)) > 0 ) {
            $name = trim($name);
            $string = preg_replace('#.*\s([\w-]*)$#', '$1', $name);
            $parts[] = $string;
            $name = trim( preg_replace('#'.$string.'#', '', $name ) );
        }

        if (empty($parts)) {
            return false;
        }

        $parts = array_reverse($parts);
        $name = array();
        $name['first_name'] = ucfirst($parts[0]);
        $name['middle_name'] = (isset($parts[2])) ? ucfirst($parts[1]) : '';
        $name['last_name'] = (isset($parts[2])) ? ucfirst($parts[2]) : ( isset($parts[1]) ? ucfirst($parts[1]) : '');

        return $name;
    }
}



