<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ApiServiceController extends Controller
{
    //

    public function apiLogin(Request $request) {

        $data = $request->all();
        $auth_attempt = \Auth::attempt(['email' => $data['email'], 'password' => $data['password'] ]);

        if($auth_attempt){
            $user = \Auth::user();

            if(!$user->isSuperAdmin() && !$user->isAdmin()){
                return self::failure('Error: Only Admins can login');
            }

            $success['token'] =  $user->createToken('ZUULAPP')->accessToken;
            $_user = $this->herderUser($request)->getData()->success;
            $_user->token = $success['token'];
            //$_user = $temp->success;
            return self::success('Authorized: Welcome to ZUUL', [ 'success' => $success, 'user' => $_user] );
        }
        else{
            return self::failure('Error: Unauthorized');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function apisignup(Request $request) {


        $requestData = $request->all();
        $email = $requestData['email'] = ( isset($requestData['email']) && isset($requestData['email']) != null ) ? $requestData['email'] : null;
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return self::failure( "Invalid email format");
        }

        $is_user = User::where('email', $requestData['email'])->first();

        if($is_user){
            return self::failure('user already exist.');
        }

        $names = Helper::split_name($requestData["name"]);

        $requestData["first_name"] = $names["first_name"];
        $requestData["middle_name"] = $names["middle_name"];
        $requestData["last_name"] = $names["last_name"];
        $requestData['email'] = isset($requestData['email']) ? $requestData['email'] : null;
        $requestData['password'] = ($requestData['password']) ? bcrypt($requestData['password']) : null;

        $email_validator = Validator::make($request->all(),[
            'name' => 'required',
            'email' => 'required:users',
            'password' => 'required'
        ]);

        if ($email_validator->fails()) {
            self::failure($email_validator->messages());
        }


        $user = User::create($requestData);
        $user->roles()->sync(7);



            return ($user) ?
                self::success('Signup Successful', ['user_id' => $user->id]) :
                self::failure('Operation failed. Please try again.');







    }
}
