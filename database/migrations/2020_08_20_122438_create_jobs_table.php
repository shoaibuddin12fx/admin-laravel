<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->id();
            $table->string('delivery_address');
            $table->double('delivery_latitude');
            $table->double('delivery_longitude');
            $table->string('description');
            $table->string('expected_delivery_time');
            $table->string('geohash');
            $table->string('item_category');
            $table->string('job_address');
            $table->double('job_latitude');
            $table->double('job_longitude');
            $table->double('job_price');
            $table->string('package_size');
            $table->string('posted_at');
            $table->string('posted_id');
            $table->string('priority');
            $table->string('receiver_instructions');
            $table->string('security_code');
            $table->string('status');          
            $table->string('document_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
