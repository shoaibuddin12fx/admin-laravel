<?php

use Spatie\Permission\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rolesArray = [
            'SuperAdmin', 'Admin', 'Driver', 'Consumer', 'User'
        ];

        //
        for($i = 0; $i < count($rolesArray); $i++) {
            Role::create([
                'name' => $rolesArray[$i],
            ]);
        }
    }
}
