<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user = User::create([
            'first_name' => 'Kranthi',
            'last_name' => 'Kandagatla',
            'email' => 'krantisruj@gmail.com',
            'password' => bcrypt("admin123"),
        ]);

        $user->assignRole('SuperAdmin');


    }
}
